%% This is the code to caculate the price of call and put option and their Greeks
% Every input in this m-file can be vector
% Inputs are (S, K, sigma, r, T)
% S:underlying price
% K:Strike price
% r:Risk-free rate
% sigma:Volatility
% T:Tenor
% call_price(S, K, sigma, r, T)
% put_price(S, K, sigma, r, T)
d1 = @(S, K, sigma, r, T)(log(S./K) + (r + 0.5*sigma.^2).*T)./(sigma.*sqrt(T));
%d1(S, K, sigma, r, T)

d2 = @(S, K, sigma, r, T)d1(S, K, sigma, r, T) - (sigma.*sqrt(T))
%d2(S, K, sigma, r, T)
call_price = @(S, K, sigma, r, T)S.*normcdf(d1(S, K, sigma, r, T)) - K.*exp(-r.*T).*normcdf(d2(S, K, sigma, r, T))
put_price = @(S, K, sigma, r, T)K.*exp(-r.*T).*normcdf(-d2(S, K, sigma, r, T)) - S.*normcdf(-d1(S, K, sigma, r, T))

call_delta = @(S, K, sigma, r, T)normcdf(d1(S, K, sigma, r, T))
%call_delta(S, K, sigma, r, T)

put_delta = @(S, K, sigma, r, T)normcdf(d1(S, K, sigma, r, T)) - 1
%put_delta(S, K, sigma, r, T)

gamma = @(S, K, sigma, r, T)normpdf(d1(S, K, sigma, r, T))./(S.*sigma.*sqrt(T))
%gamma(S, K, sigma, r, T)

vega = @(S, K, sigma, r, T)S.*normpdf(d1(S, K, sigma, r, T)).*sqrt(T)
%vega(S, K, sigma, r, T)

call_theta = @(S, K, sigma, r, T)(-(S.*normpdf(d1(S, K, sigma, r, T)).*sigma)./(2.*sqrt(T))) - r.*K.*exp(-r.*T).*normcdf(d2(S, K, sigma, r, T));
%call_theta(S, K, sigma, r, T)

put_theta = @(S, K, sigma, r, T)-(S.*normpdf(d1(S, K, sigma, r, T)).*sigma)/(2.*sqrt(T)) + r.*K.*exp(-r.*T).*normcdf(-d2(S, K, sigma, r, T));
%put_theta(S, K, sigma, r, T)

call_rho = @(S, K, sigma, r, T)K.*T.*exp(-r.*T).*normcdf(d2(S, K, sigma, r, T))
%call_rho(S, K, sigma, r, T)

put_rho = @(S, K, sigma, r, T)-K.*T.*exp(-r.*T).*normcdf(-d2(S, K, sigma, r, T))
%put_rho(S, K, sigma, r, T)

%% Example
S = [50, 60, 70];
K = [50, 60, 70];
sigma = [0.5, 0.5, 0.5];
r = [0.05, 0.05, 0.05];
T = [1, 1, 1];
callpx = call_price(S, K, sigma, r, T)
putpx = put_price(S, K, sigma, r, T)
call_delta(S, K, sigma, r, T)
call_rho(S, K, sigma, r, T)
call_theta(S, K, sigma, r, T)
put_delta(S, K, sigma, r, T)
put_rho(S, K, sigma, r, T)
put_theta(S, K, sigma, r, T)
gamma(S, K, sigma, r, T)
vega(S, K, sigma, r, T)


